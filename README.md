## Control de procesos

Resolución de los ejercicios del curso de posgrado "Control de procesos I".

El .pdf de la raíz contiene el detalle completo de las resoluciones, mientras que las carpetas "julia" y "matlab" los scripts utilizados. 