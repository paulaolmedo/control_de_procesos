%% Continuaci?n problema 1.1

G(1, 1) = tf(12.8, [16.7 1], 'iodelay', 1);
G(1, 2) = tf(-18.9, [21 1], 'iodelay', 3);
G(2, 1) = tf(6.6, [10.9 1], 'iodelay', 7);
G(2, 2) = tf(-19.4, [14.4 1], 'iodelay', 3);

G.InputName = {'R', 'S'};
G.OutputName = {'x_D', 'x_B'};

Kp1 = 0.472 * 0.45;
Ti1 = 19 * 0.83;

Kp2 = -0.616 * 0.45;
Ti2 = 10 * 0.83;

C(1, 1) = tf(0);
C(1, 2) = tf(Kp1*[Ti1 1], [Ti1 0]);
C(2, 1) = tf(Kp2*[Ti2 1], [Ti2 0]);
C(2, 2) = tf(0);
C.InputName = {'e_D', 'e_B'};
C.OutputName = {'R', 'S'};

sum_D = sumblk('e_D = r_D - x_D');
sum_B = sumblk('e_B = r_B - x_B');

sys = connect(G, C, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});

t = 0:0.1:50;
u = [ones(size(t)); ones(size(t))];
lsim(sys, u, t);


