%% Continuaci?n problema 1.2

G(1, 1) = tf(-2.2, [7 1], 'iodelay', 1);
G(1, 2) = tf(1.3, [7 1], 'iodelay', 0.3);
G(2, 1) = tf(-2.8, [9.5 1], 'iodelay', 1.8);
G(2, 2) = tf(4.3, [9.2 1], 'iodelay', 0.35);

G.InputName = {'R', 'S'};
G.OutputName = {'x_D', 'x_B'};

Kp1 = -5.333 * 0.45;
Ti1 = 3.4 * 0.83;

Kp2 = 9.751 * 0.45;
Ti2 = 1.3 * 0.83;

C(1, 1) = tf(Kp1*[Ti1 1], [Ti1 0]);
C(1, 2) = tf(0);
C(2, 1) = tf(0);
C(2, 2) = tf(Kp2*[Ti2 1], [Ti2 0]);
C.InputName = {'e_D', 'e_B'};
C.OutputName = {'R', 'S'};

sum_D = sumblk('e_D = r_D - x_D');
sum_B = sumblk('e_B = r_B - x_B');

sys = connect(G, C, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});

t = 0:0.1:50;
u = [0.1*ones(size(t)); zeros(1, floor(length(t)/2)) 0.1*ones(size(t)-floor(size(t)/2))];
lsim(sys, u, t);
