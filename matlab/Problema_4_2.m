%% Problema 4.2
clc; clear; close all;

% Definici?n de planta
G(1, 1) = tf(12.8, [16.7 1], 'iodelay', 1);
G(1, 2) = tf(-18.9, [21 1], 'iodelay', 3);
G(2, 1) = tf(6.6, [10.9 1], 'iodelay', 7);
G(2, 2) = tf(-19.4, [14.4 1], 'iodelay', 3);
G.InputName = {'R', 'S'};
G.OutputName = {'x_D', 'x_B'};

% Parte invertible 
G_inv(1, 1) = tf(12.8, [16.7 1]);
G_inv(1, 2) = tf(-18.9, [21 1]);
G_inv(2, 1) = tf(6.6, [10.9 1]);
G_inv(2, 2) = tf(-19.4, [14.4 1]);
G_inv.InputName = {'R', 'S'};
G_inv.OutputName = {'x0_D', 'x0_B'};


lambda1 = 2; lambda2 = 4;
Kp1 = 12.8; T1 = 16.7; theta1 = 1;
Kp2 = -19.4; T2 = 14.4; theta2 = 3;

Kr1 = T1/(Kp1*(lambda1+theta1));
Ti1 = T1;

Kr2 = T2/(Kp2*(lambda2+theta2));
Ti2 = T2;

% matriz diagonal de controladores PI
C(1, 1) = tf(Kr1*[Ti1 1], [Ti1 0]);
C(1, 2) = 0;
C(2, 1) = 0;
C(2, 2) = tf(Kr2*[Ti2 1], [Ti2 0]);
C.InputName = {'e_D', 'e_B'};
C.OutputName = {'R', 'S'};

S = eye(size(G*C))+G*C;

w = 1 + G_inv*C;
Wp = [w(1, 1) 0; 0 w(2, 2)];

N = Wp/S;

%% procedimiento de ajuste
omega = logspace(-1, 1, 1000);
npoints = length(omega);
s = 1i*omega;
N_infty = zeros(1, length(omega));

for j = 1:npoints
    X = evalfr(N, s(j));
    S = svd(X);
    N_infty(j) = norm(S, Inf);    
end
Lc_max = max(N_infty(:));

% Visualizo resultados
semilogx(omega, N_infty, 'b');
grid
xlabel('\omega'), ylabel('|N|_\infty')
%title('Diagrama de Bode');

% Muestro los valores finales de los par??metros
fprintf('\nN_max = %0.4f\r', Lc_max);
fprintf('\nPar??metros finales lazo X_D => R\r');
fprintf('Kc = %0.4f, Ti = %0.4f\r', Kr1, Ti1);
fprintf('\nPar??metros finales lazo X_B => S\r');
fprintf('Kc = %0.4f, Ti = %0.4f\r', Kr2, Ti2);
 
%% comparativa con controlador de Luyben
Kr1 = 0.37205;
Ti1 = 8.2804;
Kr2 = -0.077165;
Ti2 = 22.86;

C_LUYBEN(1, 1) = tf(Kr1*[Ti1 1], [Ti1 0]);
C_LUYBEN(1, 2) = 0;
C_LUYBEN(2, 1) = 0;
C_LUYBEN(2, 2) = tf(Kr2*[Ti2 1], [Ti2 0]);
C_LUYBEN.InputName = {'e_D', 'e_B'};
C_LUYBEN.OutputName = {'R', 'S'};


sum_D = sumblk('e_D = r_D - x_D');
sum_B = sumblk('e_B = r_B - x_B');

sys_L = connect(G, C_LUYBEN, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});

sys_IMC = connect(G, C, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});

t = 0:0.1:400;
u = [0.1*ones(size(t)); zeros(1, floor(length(t)/2)) 0.1*ones(size(t)-floor(size(t)/2))];
figure(2)
lsim(sys_L, sys_IMC, 'r', u, t);
axis([0 400 -0.05 0.15])
legend('Luyben', 'IMC', 'Location', 'southeast');