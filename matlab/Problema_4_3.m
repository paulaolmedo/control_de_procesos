%% Problema 4.4
clc; clear; %close all;

% Columna de destilaci??n de Wood and Berry
G(1, 1) = tf(12.8, [16.7 1], 'iodelay', 1);
G(1, 2) = tf(-18.9, [21 1], 'iodelay', 3);
G(2, 1) = tf(6.6, [10.9 1], 'iodelay', 7);
G(2, 2) = tf(-19.4, [14.4 1], 'iodelay', 3);
G.OutputName = {'x_D', 'x_B'};
G.InputName = {'R', 'S'};

Gud(1, 1) = tf(12.8, [16.7 1]);
Gud(1, 2) = tf(-18.9, [21 1]);
Gud(2, 1) = tf(6.6, [10.9 1]);
Gud(2, 2) = tf(-19.4, [14.4 1]);

%% Controlador PI
% Options = pidtuneOptions('PhaseMargin',90);
%C = pidtune(G(2,2),pidstd(2,2),0.08,Options);
Kr1 = 0.11; Ti1 = 19.8;
Kr2 = 25; Ti2 = 14.4;

% Armo la matriz diagonal de controladores PI
C(1, 1) = tf(Kr1*[Ti1 1], [Ti1 0]);
C(1, 2) = 0;
C(2, 1) = 0;
C(2, 2) = tf(Kr2*[Ti2 1], [Ti2 0]);
C.InputName = {'e_D', 'e_B'};
C.OutputName = {'R', 'S'};

sys_intermedio = feedback(G(1,1)*C(1,1), 1);
step(sys_intermedio)

% sisotool('rlocus', Gud(1,1))
% figure(1)
rlocus(C(1, 1)*Gud(1, 1))
rlocfind(C(1, 1)*Gud(1, 1))

%% Predictor de Smith
Csp(1, 1) = C(1, 1)/(1+(C(1, 1)*(Gud(1, 1)-G(1, 1))));
Csp(1, 2) = 0;
Csp(2, 1) = 0;
Csp(2, 2) = C(2, 2)/(1+(C(2, 2)*(Gud(2, 2)-G(2, 2))));
Csp.InputName = {'e_D', 'e_B'};
Csp.OutputName = {'R', 'S'};

%% Simulaci??n
sum_D = sumblk('e_D = r_D - x_D');
sum_B = sumblk('e_B = r_B - x_B');

%sys = connect(G, C, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});
sys_sp = connect(G, Csp, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});

figure(2)
t = 0:0.1:50;
u = [ones(size(t)); ones(size(t))];
%u = [0.1*ones(size(t)); zeros(1, floor(length(t)/2)) 0.1*ones(size(t)-floor(size(t)/2))];
lsim(sys_sp, 'g',u, t);
hold on
title(''); ylabel('Amplitud'); xlabel('Tiempo')
legend('Luyben', 'Monica', 'SP', 'Location', 'southeast')
%print -dsvg 9_2.svg