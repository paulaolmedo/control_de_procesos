%%% RESOLUCION PROBLEMA 4.1
%%% determinacion de parametros para controlador basado en BLT


%% definci?n planta
%% columna de destilacion de vinante&luyben
G(1, 1) = tf(-2.2, [7 1], 'iodelay', 1);
G(1, 2) = tf(1.3, [7 1], 'iodelay', 0.3);
G(2, 1) = tf(-2.8, [9.5 1], 'iodelay', 1.8);
G(2, 2) = tf(4.3, [9.2 1], 'iodelay', 0.35);
G.OutputName = {'x_D', 'x_B'};
G.InputName = {'R', 'S'};

%% simulacion ganancia critica

Kru = -5.333;
Pu = 3.4;

Kru2 = 9.751;
Pu2 = 1.3; 

%C_U(1, 1) = tf(Kru); %tf(Kru);
%C_U(1, 2) = tf(0);
%C_U(2, 1) = tf(0);
%C_U(2, 2) = tf(0);
%C_U.InputName = {'e_D', 'e_B'};
%C_U.OutputName = {'R', 'S'};

%sum_D = sumblk('e_D = r_D - x_D');
%sum_B = sumblk('e_B = r_B - x_B');
%sys = connect(G, C_U, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});
%t = 0:0.1:10;
%u = [ones(size(t)); ones(size(t))];
%lsim(sys, u, t);


%% definicion del controlador PI segun BLT - Luyben

omega = logspace(-1, 1, 100);
npoints = length(omega);
s = 1i*omega;
Lc = zeros(2, length(omega));

%%%%%
for i = 1:2
    % Asignacion del factor de desajuste
    F = i+1;
    
    % Calculo los valores de los n controladores PI
    Kp = (Kru / 2.2) / F;
    Ti = (Pu / 1.2) * F;

    Kp2 = (Kru2 / 2.2) / F;
    Ti2 = (Pu2 / 1.2) * F;

    C_LUYBEN(1, 1) = tf(Kp*[Ti 1], [Ti 0]);
    C_LUYBEN(1, 2) = 0;
    C_LUYBEN(2, 1) = 0;
    C_LUYBEN(2, 2) = tf(Kp2*[Ti2 1], [Ti2 0]);
    C_LUYBEN.InputName = {'e_D', 'e_B'};
    C_LUYBEN.OutputName = {'R', 'S'};
    
    for j = 1:npoints
        % Calculo w
        GC = evalfr(G*C_LUYBEN, s(j));
        w = -1 + det(eye(size(GC)) + GC);

        % Calculo de Lc
        Lc(i, j) = 20*log10(abs(w/(1+w)));
    end
    
    fprintf('F = %0.4f, Lc_max = %0.4f\r', F, max(Lc(i, :)));
    fprintf('\n----------------------------\r');
end

Lc_max = max(Lc(2, :));

% Visualizo resultados
L_max = 4*ones(size(omega));
semilogx(omega, L_max, '--k', omega, Lc(1, :), 'b', omega, Lc(2, :), 'r');
grid
xlabel('\omega'), ylabel('|L_{c}(j\omega)|')
%title('Diagrama de Bode');
legend('cota 4dB', 'F = 2', 'F = 3', 'Location', 'Southwest')

% Valores finales de los par??metros
fprintf('\nLc max = %0.4f\r', Lc_max);
fprintf('\nParametros finales lazo X_D => R\r');
fprintf('Kp = %0.4f, Ti = %0.4f\r', Kp, Ti);
fprintf('\nParametros finales lazo X_B => S\r');
fprintf('Kp = %0.4f, Ti = %0.4f\r', Kp2, Ti2);

%Lc max = 2.5781
%Parametros finales lazo X_D => R
%Kp = -0.8080, Ti = 8.5000
%Parametros finales lazo X_B => S
%Kp = 1.4774, Ti = 3.2500

sum_D = sumblk('e_D = r_D - x_D');
sum_B = sumblk('e_B = r_B - x_B');
sysL = connect(G, C_LUYBEN, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});
%t = 0:0.1:25;
%u = [ones(size(t)); ones(size(t))];
t = 0:0.1:50;
u = [1*ones(size(t)); zeros(1, floor(length(t)/2)) 1*ones(size(t)-floor(size(t)/2))];
lsim(sysL, u, t);


%% definicion del controlador PID segun BLT - Monica

for i = 1:3 
    % Asignar factor de desajuste
    F = i+1;
    
    % Nuevos valores de los n controladores PID
    Kp = (Kru * 0.6) / F;
    Ti = (Pu * 0.5) * F;
    Td = (Pu * 0.125) / F;

    Kp2 = (Kru2 * 0.6) / F;
    Ti2 = (Pu2 * 0.5) * F;
    Td2 = (Pu2 *0.125) / F;

    C_MONICA(1, 1) = tf(Kp*[2*Ti*Td Ti+Td 1], [Ti*Td Ti 0]);
    C_MONICA(1, 2) = 0;
    C_MONICA(2, 1) = 0;
    C_MONICA(2, 2) = tf(Kp2*[2*Ti2*Td2 Ti2+Td2 1], [Ti2*Td2 Ti2 0]);
    C_MONICA.InputName = {'e_D', 'e_B'};
    C_MONICA.OutputName = {'R', 'S'};
    
    for j = 1:npoints
        GC = evalfr(G*C_MONICA, s(j));
        w = -1 + det(eye(size(GC)) + GC);
        
        Lc(i, j) = 20*log10(abs(w/(1+w)));
    end
    
    fprintf('F = %0.4f, Lc_max = %0.4f\r', F, max(Lc(i, :)));
    fprintf('\n----------------------------\r');
end

Lc_max = max(Lc(3, :));

L_max = 4*ones(size(omega));
semilogx(omega, L_max, '--k', omega, Lc(1, :), 'b', omega, Lc(2, :), 'r', omega, Lc(3, :), 'g');
grid
xlabel('\omega'), ylabel('|L_{c}(j\omega)|')
%title('Diagrama de Bode');
legend('cota 4dB', 'F = 2', 'F = 3', 'F = 4', 'Location', 'Southwest')

% Muestro los valores finales de los par??metros
fprintf('\nLc max = %0.4f\r', Lc_max);
fprintf('\nPar??metros finales lazo X_D => R\r');
fprintf('Kp = %0.4f, Ti = %0.4f, Td = %0.4f\r', Kp, Ti, Td);
fprintf('\nPar??metros finales lazo X_B => S\r');
fprintf('Kp = %0.4f, Ti = %0.4f, Td = %0.4f\r', Kp2, Ti2, Td2);

%Lc max = 2.9642
%Par??metros finales lazo X_D => R
%Kp = -0.8000, Ti = 6.8000, Td = 0.1062
%Par??metros finales lazo X_B => S
%Kp = 1.4626, Ti = 2.6000, Td = 0.0406

sum_D = sumblk('e_D = r_D - x_D');
sum_B = sumblk('e_B = r_B - x_B');
sysM = connect(G, C_MONICA, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});
t = 0:0.1:100;
u = [ones(size(t)); ones(size(t))];
%t = 0:0.1:50;
%u = [1*ones(size(t)); zeros(1, floor(length(t)/2)) 1*ones(size(t)-floor(size(t)/2))];
lsim(sysM, u, t);


%% comparaci?n entre Luyben y Monica

t = 0:0.1:50;
u = [1*ones(size(t)); zeros(1, floor(length(t)/2)) 1*ones(size(t)-floor(size(t)/2))];
lsim(sysL, sysM, u, t);
hold on
%title('Respuestas de los controladores PI y PID')
%axis([0 400 -0.05 0.15])
legend('Luyben', 'Monica', 'Location', 'southeast')


%%% PROBLEMA 4.3 - IMC

%% parte invertible del sistema
G_inv(1, 1) = tf(-2.2, [7 1]);%, 'iodelay', 1);
G_inv(1, 2) = tf(1.3, [7 1]);%, 'iodelay', 0.3);
G_inv(2, 1) = tf(-2.8, [9.5 1]);%, 'iodelay', 1.8);
G_inv(2, 2) = tf(4.3, [9.2 1]);%, 'iodelay', 0.35);

% Obtenci?n de par?metros iniciales. Lambdas calculados seg?n Chien y
% Fruehauf (ver apunte grado)
Kp1 = -2.2; T1 = 7; theta1 = 1; lambda1 = 2;
Kp2 = 4.3; T2 = 9.2; theta2 = 0.35; lambda2 = 1;

Kr1 = T1/(Kp1*(lambda1+theta1));
Ti1 = T1;

Kr2 = T2/(Kp2*(lambda2+theta2));
Ti2 = T2;

% Armo la matriz diagonal de controladores PI
C(1, 1) = tf(Kr1*[Ti1 1], [Ti1 0]);
C(1, 2) = 0;
C(2, 1) = 0;
C(2, 2) = tf(Kr2*[Ti2 1], [Ti2 0]);
C.InputName = {'e_D', 'e_B'};
C.OutputName = {'R', 'S'};

S = eye(size(G*C))+G*C;

w = 1 + G_inv*C;
Wp = [w(1, 1) 0; 0 w(2, 2)];

N = Wp/S;

%% Inicio el procedimiento de ajuste
omega = logspace(-1, 1, 1000);
npoints = length(omega);
s = 1i*omega;
N_infty = zeros(1, length(omega));

for j = 1:npoints

    X = evalfr(N, s(j));
    S = svd(X);
    N_infty(j) = norm(S, Inf);
    
end
Lc_max = max(N_infty(:));

% Visualizo resultados
semilogx(omega, N_infty, 'b');
grid
xlabel('\omega'), ylabel('|N|_\infty')
%title('Diagrama de Bode');

fprintf('\nN_max = %0.4f\r', Lc_max);
fprintf('\nPar??metros finales lazo X_D => R\r');
fprintf('Kc = %0.4f, Ti = %0.4f\r', Kr1, Ti1);
fprintf('\nPar??metros finales lazo X_B => S\r');
fprintf('Kc = %0.4f, Ti = %0.4f\r', Kr2, Ti2);

%N_max = 2.5708
%Par??metros finales lazo X_D => R
%Kc = -1.0606, Ti = 7.0000
%Par??metros finales lazo X_B => S
%Kc = 1.5848, Ti = 9.2000

sum_D = sumblk('e_D = r_D - x_D');
sum_B = sumblk('e_B = r_B - x_B');

sys_IMC = connect(G, C, sum_D, sum_B, {'r_D', 'r_B'}, {'x_D', 'x_B'});

t = 0:0.1:50;
u = [1*ones(size(t)); zeros(1, floor(length(t)/2)) 1*ones(size(t)-floor(size(t)/2))];
lsim(sys_IMC, 'g', u, t);

%Comparaci?n BLT vs IMC
lsim(sysL, sysM, sys_IMC, 'g', u, t);
legend('Luyben', 'Monica', 'IMC', 'Location', 'Northeast');


